/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.renu.midterm;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author ray
 */
public class UserService {
    private static ArrayList<Product> productList = null;
    static {
        productList = new ArrayList<>();
        // Mock data
        productList.add(new Product("ID","Name","Brand",22,12));
        productList.add(new Product("ID2","Name2","Brand2",22,12));
        productList.add(new Product("ID2","Name3","Brand3",22,12));
     }
    //create (C)
    public static boolean addProduct(Product product){
        productList.add(product);
        
        return true;
    }
    // Delete (D)
    public static boolean delProduct(Product product){
        productList.remove(product);
        
        return true;
    }
    public static boolean delProduct(int index){
        productList.remove(index);
       
        return true;
    }
    //read (R)
    public static ArrayList<Product> getProduct(){
        return productList;
    }  
    public static Product getProduct(int index){
        return productList.get(index);
    
    }
    // Update (U)
    public static boolean updateProduct(int index, Product product){
        productList.set(index, product);
        
        return true;
    }
    public static void save(){
        
    }
}


    