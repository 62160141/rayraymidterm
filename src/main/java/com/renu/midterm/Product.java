/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.renu.midterm;

/**
 *
 * @author ray
 */
public class Product {
    private String id;
    private String name;
    private String brand;
    private double price;
    private int amount;
    
    public Product(String id, String name, String brand, double price, int amount)
    {
        this.id = id;
        this.name = name;
        this.brand = brand;
        this.price = price;
        this.amount = amount;
        
    }
    public String getId()
    {
        return id;
    }
    public String getName()
    {
    return name;
    }
    public String getBrand()
    {
        return brand;
    }
    public double getPrice()
    {
        return price;
    }
    public int getamount()
    {
        return amount;
    }
       
    public String setId()
    {
        return id;
    }
    public String setName()
    {
    return name;
    }
    public String setBrand()
    {
        return brand;
    }
    public double setgetPrice()
    {
        return price;
    }
    public int setamount()
    {
        return amount;
    
    }
  public String toString() {
        return "Id : " + id + " " + "Name : " + name + " " + "Brand : " 
                + brand + " " + "Price : " + price + " " + "Amount : " + amount;
    }
    }

